import React from 'react'
import { Pagination } from 'react-bootstrap';
export default function PaginationList({ perPage, totalPost, paginate, nextPage, previousPage, currentPage }) {
   let pageNumber = [];
   for (let number = 1; number <= Math.ceil(totalPost / perPage); number++) {
      pageNumber.push(number);
   }
   const style = {
      display: 'flex',
      justifyContent: 'center',
      padding: "20px 0"
   }
   return (
      <Pagination style={style} >
         <Pagination.Item className={currentPage === 1 ? " page-item disabled" : "page-item"} onClick={previousPage} >Previous</Pagination.Item>
         {pageNumber.map(number =>
            <Pagination.Item
               onClick={() => paginate(number)}
               key={number}
               active={number === currentPage}>
               {number}
            </Pagination.Item>)}
         <Pagination.Item className={currentPage === pageNumber.length ? " page-item disabled" : "page-item"} onClick={nextPage} >Next</Pagination.Item>
      </Pagination>
   )
}
