import React from 'react'
import { Modal, Button } from 'react-bootstrap'
import JobList from './JobList';
export default function ModalView({ onHide, show, dataView }) {
   const { name, gender, jobs, age } = dataView[0];
   return (
      <Modal show={show} onHide={onHide}>
         <Modal.Header closeButton>
            <Modal.Title>{name}</Modal.Title>
         </Modal.Header>
         <Modal.Body>
            <h6>Gender: {gender}</h6>
            <h6>Age: {age}</h6>
            <h6>Jobs : <JobList jobs={jobs} /></h6>
         </Modal.Body>
         <Modal.Footer>
            <Button variant="secondary" onClick={onHide} children="close" />
         </Modal.Footer>
      </Modal>
   )
}
