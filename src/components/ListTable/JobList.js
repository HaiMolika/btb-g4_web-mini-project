import React from 'react'

export default function JobList({ jobs }) {
   return (
      <ul>
         {jobs.map((list, index) => <li key={index}>{list}</li>)}
      </ul>
   )
}
