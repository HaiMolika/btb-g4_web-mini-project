import React, { Component } from "react";
import { Table } from "react-bootstrap";
import ListTableRow from "./ListTableRow";
import PaginationList from "../Pagination/PaginationList";

export default class ListTable extends Component {
   state = {
      currentPage: 1,
      perPage: 3,
   }
   //change page
   handleChangePage = (pageNumber) => {
      this.setState({ currentPage: pageNumber });
   };
   //next page
   handleNextPage = () => {
      this.setState({ currentPage: this.state.currentPage + 1 });
   };
   //previous page
   handlePreviousPage = () => {
      this.setState({ currentPage: this.state.currentPage - 1 });
   };
   render() {
      const {
         persons,
         onRemove,
         onUpdate,
         onView,
      } = this.props;
      let indexOfLastRow = this.state.currentPage * this.state.perPage;
      let indexOfFirstRow = indexOfLastRow - this.state.perPage;
      let currentPageList = this.props.persons.slice(
         indexOfFirstRow,
         indexOfLastRow
      );

      return (
         <React.Fragment>
            <Table bordered>
               <thead>
                  <tr>
                     <th>Id</th>
                     <th>Name</th>
                     <th>Age</th>
                     <th>Gender</th>
                     <th>Job</th>
                     <th>Created At</th>
                     <th>Updated At</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  {currentPageList.map((person) => {
                     return (
                        <ListTableRow
                           key={person.id}
                           persons={person}
                           onView={onView}
                           onRemove={onRemove}
                           onUpdate={onUpdate}
                        />
                     );
                  })}
               </tbody>
            </Table>
            <PaginationList
               perPage={this.state.perPage}
               totalPost={persons.length}
               paginate={this.handleChangePage}
               nextPage={this.handleNextPage}
               previousPage={this.handlePreviousPage}
               currentPage={this.state.currentPage}
            />
         </React.Fragment>
      );
   }

}
