import React from 'react'
import JobList from './JobList'
import Moment from 'react-moment';
export default function ListTableRow(props) {
   const { id, name, age, gender, jobs, createdTime, updatedTime, } = props.persons;
   const { onView, onUpdate, onRemove } = props;
   return (
      <tr>
         <td>{id}</td>
         <td>{name}</td>
         <td>{age}</td>
         <td>{gender}</td>
         <td>
            <JobList jobs={jobs} />
         </td>
         <td><Moment fromNow locale="km">{createdTime}</Moment></td>
         <td><Moment fromNow locale="km">{updatedTime}</Moment></td>
         <td>
            <button
               className="btn btn-info m-1"
               onClick={() => onView(id)}>View</button>
            <button
               className="btn btn-warning m-1"
               onClick={() => onUpdate(id)}>Update</button>
            <button
               className="btn btn-danger m-1"
               onClick={() => onRemove(id)}>Delete</button>
         </td>
      </tr>
   )
}

