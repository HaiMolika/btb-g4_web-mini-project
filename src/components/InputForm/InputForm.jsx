import React, { Component } from "react";
import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import {
   Container,
   Row,
   Col,
   Button,
   InputGroup,
   FormControl,
} from "react-bootstrap";
import "./InputForm.css";
import "moment/locale/km";
import ListTable from "../ListTable/ListTable";
import ListCard from "../ListCard/ListCard";
import ModalView from "../ListTable/ModalView";
export default class InputForm extends Component {
   constructor() {
      super();
      this.state = {
         id: "",
         name: "",
         gender: "",
         age: "",
         createdTime: "",
         updatedTime: "",
         jobs: [],
         persons: [],
         isShow: false,
         dataView: {},
         isChecked: false,
         isUpdate: false,
         isSubmit: false,
         isList: true,
         isChangeUpdate: false,
      };
   }
   rdoMale = React.createRef();
   rdoFemale = React.createRef();
   cboTeacher = React.createRef();
   cboStudent = React.createRef();
   cboDevelper = React.createRef();

   handledNameOnChange = (event) => {
      this.setState({ name: event.target.value });
   };

   handledAgeOnchange = (event) => {
      this.setState({ age: event.target.value });
   };

   handledGenderOnChange = (event) => {
      var target = event.target;
      if (target.checked) {
         this.setState({ gender: target.value });
      }
   };

   handledJobOnChange = (event) => {
      var target = event.target;
      const value = [...this.state.jobs];
      if (value.includes(target.value)) {
         value.splice(value.indexOf(target.value), 1);
      } else {
         value.push(target.value);
      }
      this.setState({ jobs: value });
   };

   generateID = () => {
      var characters =
         "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      var result = "";
      var charactersLength = characters.length;
      for (var i = 0; i < 5; i++) {
         result += characters.charAt(
            Math.floor(Math.random() * charactersLength)
         );
      }
      return result;
   };
   // submit
   onSubmit = () => {
      var pat1 = /[^a-z] \S/gi;
      var pat2 = /[^0-9]/gi;
      if (
         this.state.name.match(pat1) != null ||
         this.state.age.match(pat2) != null
      ) {
         alert("Name or Age is Invalid, Please input again");
      } else if (this.state.name === "" || this.state.age === "") {
         alert("Can't Empty");
      } else {
         var data = {
            id: this.generateID(),
            name: this.state.name,
            gender: this.state.gender,
            age: this.state.age,
            createdTime: new Date().toISOString(),
            updatedTime: new Date().toISOString(),
            jobs: this.state.jobs,
         };
         this.setState({
            persons: [...this.state.persons, data],
            isChecked: false,
         });
         //clear controll
         this.clearControl();
      }
   };

   clearControl = () => {
      this.setState({
         name: "",
         age: "",
         gender: "",
         jobs: [],
      });

      this.rdoMale.current.checked = false;
      this.rdoFemale.current.checked = false;
      this.cboStudent.current.checked = false;
      this.cboDevelper.current.checked = false;
      this.cboTeacher.current.checked = false;
   };
   //update row
   handleUpdate = (id) => {
      let person = this.state.persons.find((person) => person.id === id);

      if (person.gender === "Male") this.rdoMale.current.checked = true;
      else if (person.gender === "Female")
         this.rdoFemale.current.checked = true;
      let jobs = [];

      if (this.state.isChangeUpdate) {
         this.cboStudent.current.checked = false;
         this.cboTeacher.current.checked = false;
         this.cboDevelper.current.checked = false;
      }
      person.jobs.map((job) => {
         if (job === "Developer") {
            this.cboDevelper.current.checked = true;
            jobs.push(job);
         }
         if (job === "Teacher") {
            this.cboTeacher.current.checked = true;
            jobs.push(job);
         }
         if (job === "Student") {
            this.cboStudent.current.checked = true;
            jobs.push(job);
         }
         return jobs;
      });

      this.setState({
         id: person.id,
         name: person.name,
         age: person.age,
         gender: person.gender,
         jobs: jobs,
         isUpdate: true,
         isChangeUpdate: true,
      });
   };
   onUpdate = (id) => {
      let person = this.state.persons.find((person) => person.id === id);

      person.name = this.state.name;
      person.age = this.state.age;
      person.updatedTime = new Date().toISOString();
      person.gender = this.state.gender;
      person.jobs = this.state.jobs;

      this.setState({
         isUpdate: !this.state.isUpdate,
      });
      this.clearControl();
   };
   //remove row
   handleRemove = (id) => {
      let persons = this.state.persons.filter((p) => p.id !== id);
      this.setState({ persons });
   };

   //view modal
   handleView = (id) => {
      const persons = this.state.persons.filter((person) => person.id === id);
      this.setState({ isShow: true, dataView: persons });
   };
   //close modal
   handleClose = () => {
      this.setState({ isShow: false });
   };
   render() {
      return (
         <div>
            <h1 className="title">Personal Information</h1>
            <Container fluid={true}>
               <Row>
                  <Col md="8">
                     <p className="set-par">Name : </p>
                     <InputGroup className="mb-3">
                        <FormControl
                           aria-label="Text input with checkbox"
                           placeholder="Input Name"
                           value={this.state.name}
                           onChange={this.handledNameOnChange}
                        />
                     </InputGroup>
                     <p className="set-par">Age : </p>
                     <InputGroup className="mb-3">
                        <FormControl
                           aria-label="Text input with checkbox"
                           placeholder="Age"
                           value={this.state.age}
                           onChange={this.handledAgeOnchange}
                        />
                     </InputGroup>
                     <div>
                        {this.state.isUpdate ? (
                           <Button
                              variant="dark"
                              onClick={() => this.onUpdate(this.state.id)}
                              children="Update"
                           />
                        ) : (
                              <Button
                                 variant="dark"
                                 onClick={this.onSubmit}
                                 children="Submit"
                              />
                           )}
                     </div>
                  </Col>
                  <Col md="4">
                     <h4 className="set-h4">Gender : </h4>
                     <label className="container">
                        Male
                        <input
                           type="radio"
                           name="gender"
                           value="Male"
                           ref={this.rdoMale}
                           onChange={this.handledGenderOnChange.bind(this)}
                        />
                        <span className="circle"></span>
                     </label>
                     <label className="container">
                        Female
                        <input
                           type="radio"
                           name="gender"
                           value="Female"
                           ref={this.rdoFemale}
                           onChange={this.handledGenderOnChange.bind(this)}
                        />
                        <span className="circle"></span>
                     </label>
                     <h4 className="set-h4">Job : </h4>
                     <label className="container">
                        Student
                        <input
                           type="checkbox"
                           value="Student"
                           ref={this.cboStudent}
                           onChange={this.handledJobOnChange.bind(this)}
                        />
                        <span className="checkMark"></span>
                     </label>
                     <label className="container">
                        Teacher
                        <input
                           type="checkbox"
                           value="Teacher"
                           ref={this.cboTeacher}
                           onChange={this.handledJobOnChange.bind(this)}
                        />
                        <span className="checkMark"></span>
                     </label>
                     <label className="container">
                        Developer
                        <input
                           type="checkbox"
                           value="Developer"
                           ref={this.cboDevelper}
                           onChange={this.handledJobOnChange.bind(this)}
                        />
                        <span className="checkMark"></span>
                     </label>
                  </Col>
               </Row>
               <div>
                  <center className="pb-3">
                     <h5>Display data : </h5>
                     <Button
                        variant="outline-dark"
                        className="set-button"
                        onClick={() => this.setState({ isList: true })}
                        children="List"
                     />

                     <Button
                        variant="outline-dark"
                        onClick={() => this.setState({ isList: false })}
                        children="Card"
                     />
                  </center>
                  {this.state.persons.length !== 0 && this.state.isList ?
                     <ListTable
                        time={this.state.time}
                        persons={this.state.persons}
                        onRemove={this.handleRemove}
                        onUpdate={this.handleUpdate}
                        onView={this.handleView}
                     />
                     : this.state.persons.length !== 0 ?
                        <ListCard
                           persons={this.state.persons}
                           onRemove={this.handleRemove}
                           onUpdate={this.handleUpdate}
                           onView={this.handleView}
                        /> : ''
                  }
               </div>
            </Container>
            {this.state.isShow &&
               ReactDOM.createPortal(
                  <ModalView
                     show={this.state.isShow}
                     onHide={this.handleClose}
                     dataView={this.state.dataView}
                  />,
                  document.getElementById("root")
               )}
         </div>
      );
   }
}
