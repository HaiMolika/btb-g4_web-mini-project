import React, { Component } from "react";
import { Card, Dropdown } from "react-bootstrap";
import JobList from "../ListTable/JobList";
import Moment from "react-moment";
import PaginationList from "../Pagination/PaginationList";

export default class ListCard extends Component {
  state = {
    currentPage: 1,
    perPage: 8,
  }
  //change page
  handleChangePage = (pageNumber) => {
    this.setState({ currentPage: pageNumber });
  };
  //next page
  handleNextPage = () => {
    this.setState({ currentPage: this.state.currentPage + 1 });
  };
  //previous page
  handlePreviousPage = () => {
    this.setState({ currentPage: this.state.currentPage - 1 });
  };
  render() {
    const { persons, onRemove, onUpdate, onView } = this.props;

    let indexOfLastRow = this.state.currentPage * this.state.perPage;
    let indexOfFirstRow = indexOfLastRow - this.state.perPage;
    let currentPage = this.props.persons.slice(
      indexOfFirstRow,
      indexOfLastRow
    );

    const listCard = currentPage.map((person) => (
      <div className="col-md-3 mb-4" key={person.id} >
        <Card className="h-100" style={{ fontWeight: "bold" }} >
          <Card.Header className="text-center">
            <Dropdown>
              <Dropdown.Toggle variant="info" id="dropdown-basic">
                Action
                     </Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item onClick={() => onView(person.id)} >View</Dropdown.Item>
                <Dropdown.Item onClick={() => onUpdate(person.id)}>Update</Dropdown.Item>
                <Dropdown.Item onClick={() => onRemove(person.id)}>Delete</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Card.Header>
          <Card.Body>
            <h3>{person.name}</h3>
            <h4>Jobs:</h4>
            <h6>
              <JobList jobs={person.jobs} />
            </h6>
          </Card.Body>
          <Card.Footer className="text-muted text-center">
            <Moment fromNow locale="km">
              {persons.createdTime}
            </Moment>
          </Card.Footer>
        </Card>
      </div>
    ));
    return (

      <React.Fragment>
        <div className="row">{listCard}  </div>
        <PaginationList
          perPage={this.state.perPage}
          totalPost={persons.length}
          paginate={this.handleChangePage}
          nextPage={this.handleNextPage}
          previousPage={this.handlePreviousPage}
          currentPage={this.state.currentPage}
        />
      </React.Fragment>
    )
  }
}
