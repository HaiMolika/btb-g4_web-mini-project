import React from 'react';
import './App.css';
import InputForm from './components/InputForm/InputForm'

function App() {
  return (
    <div style={{ width: '90%', margin: 'auto' }}>
      <InputForm />
    </div>

  );
}

export default App;
